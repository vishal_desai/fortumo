package ee.kmd.at;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import ee.kmd.rest.services.AsyncControllerService;
import ee.kmd.rest.services.AsyncControllerServiceImpl;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;

/**
 * Created by Vishal on 18/10/2015.
 */

@RunWith(Cucumber.class)
@CucumberOptions(format="html:target/cucumber", features = "src/test/java/ee/kmd/at/features")
public class CukeRunner {

    @Bean
    public AsyncControllerService asyncControllerService() {
        return new AsyncControllerServiceImpl();
    }
}