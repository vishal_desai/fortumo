  Feature: Asynchronous request handling
    As ES
    I wish to make an Asynchronous request

    Scenario: IS handles Asynchronous GET request
      When ES sends an Asynchronous request to IS
      Then ES has received a Callback request from IS up until 11 seconds
