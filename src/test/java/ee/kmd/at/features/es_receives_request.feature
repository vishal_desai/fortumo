Feature: ES receives a request
  As a Client
  I wish to make a Synchronous request

  Background: Timeout configuration
    Given ES has set timeout as 5 seconds

  Scenario: Client makes a proper request
    When Client sends a proper request
    Then Client receives a 200 response

  Scenario: Client makes an Improper request
    When Client sends an improper request
    Then Client receives a 404 response