package ee.kmd.at.features;

import ee.kmd.DemoApplication;
import ee.kmd.rest.dtos.ResponseDto;
import ee.kmd.rest.services.AsyncControllerService;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Vishal on 19/10/2015.
 */

@ContextConfiguration(classes = DemoApplication.class, loader = SpringApplicationContextLoader.class)
@RunWith(SpringJUnit4ClassRunner.class)
@WebIntegrationTest
@WebAppConfiguration
public class BaseSteps {

    protected static final String SEARCH_URL_IS = "http://localhost:8080/backend-service/method";
    protected static final String SEARCH_URL_ES = "http://localhost:8080/service/method";
    protected static final String SEARCH_URL_ES_IMPROPER = "http://localhost:8080/service1/method";
    protected static final String POST_URL_ES = "http://localhost:8080/callback.service/method";

    protected RestTemplate restTemplate = new RestTemplate();

    protected MockMvc mockMvc;

    protected ResponseDto testingDto = new ResponseDto();

    @Autowired
    protected AsyncControllerService asyncControllerService;

}
