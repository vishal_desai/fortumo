Feature: ES handles Callback request
  As IS
  I wish to send a Callback request after processing an Asynchronous request

  Background:
    Given ES has set timeout as 5 seconds

    Scenario: After timeout
      Given IS will send Callback request to ES after 10 seconds
      When Client sends a proper request
      Then ES sends a 503 response to client

    Scenario: Before timeout
      Given IS will send Callback request to ES after 3 seconds
      When Client sends a proper request
      Then ES sends a 200 response to client

