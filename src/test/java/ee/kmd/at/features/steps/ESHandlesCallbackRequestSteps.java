package ee.kmd.at.features.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ee.kmd.DemoApplication;
import ee.kmd.at.features.BaseSteps;
import ee.kmd.rest.services.AsyncControllerService;
import ee.kmd.rest.sync.controllers.SynchronousController;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.google.common.collect.Maps.newHashMap;
import static ee.kmd.rest.sync.controllers.SynchronousController.setTimeout;
import static java.text.DateFormat.getInstance;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by Vishal on 18/10/2015.
 */

@ContextConfiguration(classes = DemoApplication.class, loader = SpringApplicationContextLoader.class)
@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTest
@WebAppConfiguration
public class ESHandlesCallbackRequestSteps extends BaseSteps{

    @Mock
    SynchronousController synchronousController;

    @And("^ES has set timeout as (\\d+) seconds$")
    public void ES_has_set_timeout_as_seconds(int timeout) throws Throwable {
        setTimeout(timeout);
    }

    @Then("^ES sends a (\\d+) response to client$")
    public void ES_sends_a_response(int statusCode) throws Throwable {
        assertThat(synchronousController.createResponseEntity().getStatusCode(),
                is(HttpStatus.valueOf(statusCode)));
    }

    @Given("^IS will send Callback request to ES after (\\d+) seconds$")
    public void IS_will_send_Callback_request_to_ES_after_seconds(int sleepTime) throws Throwable {
        asyncControllerService.setSleepTime(sleepTime);
    }

    @When("^IS sends a Callback request to ES$")
    public void IS_sends_a_Callback_request_to_ES() throws Throwable {
        testingDto.setTimestamp(getInstance().format(System.currentTimeMillis()));
        testingDto.setResponse(newHashMap());
        AsyncControllerService asyncControllerService = Mockito.spy(AsyncControllerService.class);
        verify(asyncControllerService, times(1)).callbackWithResponse(testingDto);
    }
}
