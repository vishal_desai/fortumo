package ee.kmd.at.features.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ee.kmd.at.features.BaseSteps;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.http.HttpStatus.valueOf;

/**
 * Created by Vishal on 18/10/2015.
 */

public class ESReceivesRequestSteps extends BaseSteps {

    ResponseEntity<?> responseEntity;
    HttpStatus httpStatus;

    @When("^Client sends a proper request$")
    public void Client_sends_a_proper_request() throws Throwable {
        responseEntity = restTemplate.getForEntity(SEARCH_URL_ES, null);
        httpStatus = responseEntity.getStatusCode();
    }

    @When("^Client sends an improper request$")
    @ExceptionHandler(HttpClientErrorException.class)
    public void Client_sends_an_improper_request() {
        try {
            responseEntity = restTemplate.getForEntity(SEARCH_URL_ES_IMPROPER, null);
            httpStatus = responseEntity.getStatusCode();
        } catch (HttpClientErrorException e) {
            httpStatus = e.getStatusCode();
        }
    }

    @Then("^Client receives a (\\d+) response$")
    public void Client_receives_a_response(int statusCode) throws Throwable {
        assertThat(httpStatus, is(valueOf(statusCode)));
    }
}
