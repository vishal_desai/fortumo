package ee.kmd.at.features.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ee.kmd.at.features.BaseSteps;
import ee.kmd.rest.sync.controllers.SynchronousController;

import java.util.concurrent.ExecutionException;

import static com.google.common.collect.Maps.newHashMap;
import static java.lang.Thread.sleep;
import static java.text.DateFormat.getInstance;
import static org.mockito.Mockito.*;

/**
 * Created by Vishal on 18/10/2015.
 */


public class ISReceivesAsyncRequestSteps extends BaseSteps {

    @When("^ES sends an Asynchronous request to IS$")
    public void es_sends_an_Asynchronous_request_to_IS() throws ExecutionException, InterruptedException {
        restTemplate.getForEntity(SEARCH_URL_IS, null);
    }

    @Then("^ES has received a Callback request from IS up until (\\d+) seconds$")
    public void ES_has_received_a_Callback_request_from_IS_up_until_seconds(int seconds) throws Throwable {
        testingDto.setTimestamp(getInstance().format(System.currentTimeMillis()));
        testingDto.setResponse(newHashMap());
        SynchronousController synchronousController = spy(SynchronousController.class);
        verify(synchronousController, times(1)).callbackPost(testingDto);
    }
}
