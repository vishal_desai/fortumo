package ee.kmd.it;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.kmd.DemoApplication;
import ee.kmd.rest.dtos.ResponseDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.WebApplicationContext;

import static com.google.common.collect.Maps.newHashMap;
import static java.text.DateFormat.getInstance;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by Vishal on 21/10/2015.
 */
@ContextConfiguration(classes = DemoApplication.class, loader = SpringApplicationContextLoader.class)
@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTest
@WebAppConfiguration
public class SynchronousControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void testReturns200() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:8080/service/method"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @ExceptionHandler(HttpClientErrorException.class)
    public void testReturns404() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:8080/service1/method"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testCallback() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        ResponseDto testingDto = new ResponseDto();
        testingDto.setTimestamp(getInstance().format(System.currentTimeMillis()));
        testingDto.setResponse(newHashMap());
        mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8080/callback.service/method")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testingDto)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
