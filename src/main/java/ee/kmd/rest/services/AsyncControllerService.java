package ee.kmd.rest.services;

import ee.kmd.rest.dtos.ResponseDto;
import org.springframework.stereotype.Service;

/**
 * Created by Vishal on 21/10/2015.
 */
@Service
public interface AsyncControllerService {

    void getAsyncResponse();

    void callbackWithResponse(ResponseDto responseDto);

    void setSleepTime(int time);
}
