package ee.kmd.rest.services;

import org.springframework.stereotype.Service;

/**
 * Created by Vishal on 21/10/2015.
 */
@Service
public interface SyncControllerService {

    void makeRequest();
}
