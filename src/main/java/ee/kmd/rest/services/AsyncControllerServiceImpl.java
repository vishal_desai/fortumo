package ee.kmd.rest.services;

import ee.kmd.rest.dtos.ResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

import static java.lang.Thread.sleep;
import static java.text.DateFormat.getInstance;
import static java.util.logging.Logger.getLogger;
import static jersey.repackaged.com.google.common.collect.Maps.newHashMap;
import static org.springframework.http.HttpStatus.OK;

/**
 * Created by Vishal on 19/10/2015.
 */


public class AsyncControllerServiceImpl implements AsyncControllerService{


    private Logger logger = getLogger(AsyncControllerServiceImpl.class.getName());

    RestTemplate restTemplate = new RestTemplate();
    private static final String CALLBACK_URL = "http://localhost:8080/callback.service/method";

    private static int sleepTime = 5000;

    public void setSleepTime(int sleepTime) {
        setSleep(sleepTime);
    }

    public static void setSleep(int sleepTime) {
        AsyncControllerServiceImpl.sleepTime = sleepTime;
    }

    @Async
    @Override
    public void getAsyncResponse() {
        ResponseDto response;
        try {
            response = createResponse();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        callbackWithResponse(response);
    }

    public ResponseDto createResponse() throws InterruptedException {
        sleep(sleepTime);
        ResponseDto responseDto = new ResponseDto();
        responseDto.setTimestamp(getInstance().format(System.currentTimeMillis()));
        responseDto.setResponse(newHashMap());
        return responseDto;
    }

    @Override
    public void callbackWithResponse(ResponseDto responseDto) {
        ResponseEntity<ResponseDto> responseDtoResponseEntity =
                restTemplate.postForEntity(CALLBACK_URL, responseDto, ResponseDto.class);
        if(!responseDtoResponseEntity.getStatusCode().equals(OK)) {
            throw new RuntimeException("Callback failed");
        }
    }
}
