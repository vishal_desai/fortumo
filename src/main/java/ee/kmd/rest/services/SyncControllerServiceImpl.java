package ee.kmd.rest.services;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Vishal on 20/10/2015.
 */
@Service
public class SyncControllerServiceImpl implements SyncControllerService{

    private static final String SEARCH_URL = "http://localhost:8080/backend-service/method";

    RestTemplate restTemplate = new RestTemplate();

    public void makeRequest() {
        restTemplate.getForEntity(SEARCH_URL, null);
    }
}
