package ee.kmd.rest.dtos;

import java.util.Map;

/**
 * Created by Vishal on 19/10/2015.
 */
public class ResponseDto {

    private String timestamp;

    private Map response;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Map getResponse() {
        return response;
    }

    public void setResponse(Map response) {
        this.response = response;
    }
}
