package ee.kmd.rest.sync.controllers;

import ee.kmd.rest.dtos.ResponseDto;
import ee.kmd.rest.services.SyncControllerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

import static java.lang.Thread.sleep;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by Vishal on 18/10/2015.
 */
@RestController
public class SynchronousController {

    @Autowired
    SyncControllerService syncControllerService;

    private static ResponseDto response;

    private static int timeout = 12000;

    public static void setTimeout(int timeout) {
        SynchronousController.timeout = timeout;
    }

    @RequestMapping(value = "service/method", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> syncGet() throws ExecutionException, InterruptedException {
        syncControllerService.makeRequest();
        int iteration = 1;
        while(response == null && iteration < 5) {
            sleep(timeout/4);
            iteration += 1;
        }
        return createResponseEntity();
    }

    @RequestMapping(value = "callback.service/method", method = POST)
    public ResponseEntity<ResponseDto> callbackPost(@RequestBody ResponseDto responseDto) {
        response = responseDto;
        return new ResponseEntity<ResponseDto>(response, OK);
    }

    public ResponseEntity<?> createResponseEntity() {
        if( response != null) {
            return new ResponseEntity<>(response, OK);
        } else {
            return new ResponseEntity<>(INTERNAL_SERVER_ERROR);
        }
    }

}
