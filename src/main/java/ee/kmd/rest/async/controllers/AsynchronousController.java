package ee.kmd.rest.async.controllers;

import ee.kmd.rest.services.AsyncControllerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by Vishal on 18/10/2015.
 */
@RestController
@RequestMapping(value = "backend-service")
public class AsynchronousController {

    @Autowired
    AsyncControllerService asyncControllerService;

    @RequestMapping(method = GET, value = "method", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> asyncGet() {
        asyncControllerService.getAsyncResponse();
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
